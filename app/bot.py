from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackContext
from pymongo import MongoClient
from datetime import datetime
from telegram import Update 

# Ваш токен бота, полученный от BotFather
TOKEN = "6728831510:AAHnttahWhyiy9h9RJAItUGjvB2_fKKIRe4"

# Подключение к базе данных MongoDB
client = MongoClient("mongodb+srv://illiatasminskyi:qwertyuiop@cluster0.bav0d4t.mongodb.net/")
db = client["jc_bot"]
users_collection = db["users"]
passwords_collection = db["passwords"]

# Глобальная переменная для диспетчера
dp = None

def start(update: Update, context: CallbackContext) -> None:
    user_id = update.message.from_user.id
    user_data = {
        "user_id": user_id,
        "username": update.message.from_user.username,
        "first_name": update.message.from_user.first_name,
        "last_name": update.message.from_user.last_name,
        "role": "user",
    }

    # Проверяем, есть ли пользователь уже в базе
    existing_user = users_collection.find_one({"user_id": user_id})
    if existing_user:
        update.message.reply_text('С возвращением!')
    else:
        # Добавляем нового пользователя в базу данных
        users_collection.insert_one(user_data)
        update.message.reply_text('Привет! Я зарегистрировал тебя в базе.')

def new_password_start(update: Update, context: CallbackContext) -> None:
    # Отправляем сообщение "Пароль:" перед вводом пароля
    update.message.reply_text('Пароль:')
    # Регистрируем новый обработчик для ожидания ввода пароля
    dp.add_handler(MessageHandler(Filters.text & ~Filters.command, new_password))

def new_password(update: Update, context: CallbackContext) -> None:
    user_id = update.message.from_user.id

    # Получаем введенный пользователем пароль
    password = update.message.text

    # Сохраняем запись о пароле в базу данных
    password_data = {
        "user_id": user_id,
        "password": password,
        "creation_date": datetime.now(),
    }
    passwords_collection.insert_one(password_data)

    update.message.reply_text('Пароль успешно сохранен!')

    # Удаляем временный обработчик после сохранения пароля
    dp.remove_handler(new_password)

def main() -> None:
    # Создание объекта Updater и передача ему токена
    updater = Updater(TOKEN)

    # Получение диспетчера для регистрации обработчиков
    global dp
    dp = updater.dispatcher

    # Регистрация обработчика команды /start
    dp.add_handler(CommandHandler("start", start))

    # Регистрация обработчика для текстовых сообщений (пароля)
    dp.add_handler(CommandHandler("new_password", new_password_start))

    # Запуск бота
    updater.start_polling()

    # Бот будет работать до принудительного завершения
    updater.idle()

if __name__ == '__main__':
    main()