FROM python:3.11

WORKDIR /app

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

COPY . .

# Install pymongo with the srv extra
# RUN pip install "pymongo[srv]"

CMD ["python", "./app/bot.py"]
